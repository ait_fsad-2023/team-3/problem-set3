Full Stack Application Development
Lab Report: Odds, Ends, and User Management
# 1. Continuous Integration with GitLab CI:
1.1 Choose GitLab CI:
We chose GitLab CI for its seamless integration with our GitLab repository.

### 1.2 Configure CI/CD:

```yaml
# .gitlab-ci.yml
stages:
  - build
  - test
  - deploy

variables:
  RAILS_ENV: test

before_script:
  - gem install bundler --no-document
  - bundle install --jobs $(nproc) --no-parallel

build:
  stage: build
  script:
    - # Add build commands

test:
  stage: test
  script:
    - bundle exec rails db:create db:schema:load
    - bundle exec rspec # Or your testing command
  coverage: '/Coverage (\d+\.\d+)/'

deploy:
  stage: deploy
  script:
    - # Add deployment commands
  only:
    - master
```

### 1.3 Implement Regression Test Suite:
We've implemented a regression test suite using RSpec for testing. FactoryBot is used for fixtures, and SimpleCov is integrated for code coverage.

# 2. Deployment Automation with Capistrano:
### 2.1 Choose Deployment Tool:
Capistrano was chosen for deployment automation.

### 2.2 Configuration for Deployment:

```ruby
# config/deploy.rb
set :application, 'your_application_name'
set :repo_url, 'git@github.com:yourusername/your_repository.git'
set :deploy_to, '/var/www/your_application_name'
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/master.key', '.env')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system', 'public/uploads', 'node_modules')

set :keep_releases, 5

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Restart your application server, e.g., for a Rails app:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart
end
```

```ruby
# config/deploy/production.rb
server 'your_server_ip', user: 'your_ssh_user', roles: %w{app db web}
```

# 3. Security Audit:
In accordance with security best practices, our project underwent a comprehensive audit to identify and address potential sensitive data exposure. The audit specifically focused on securing database passwords and other confidential information.

### Measures Taken:
1. Secure Storage:
Database passwords and other sensitive information are securely stored, following industry-standard encryption and protection mechanisms.

2. Environment Variables:
Sensitive data, such as database credentials, is not hardcoded in the source code. Instead, it is stored as environment variables to prevent accidental exposure.

3. Access Controls:
Strict access controls are enforced to restrict access to sensitive data only to authorized personnel. This includes limiting database access and utilizing role-based access control (RBAC).

4. Encryption:
Data in transit and at rest, especially sensitive data, is encrypted using secure protocols and encryption algorithms.

5. Regular Auditing:
Regular audits are scheduled to reevaluate and update security measures. This proactive approach ensures continuous protection against emerging threats.

# 4. User Data Model in Ruby on Rails:
### 4.1 Define User Data Model:
We generated a User model with the necessary attributes:

```bash
rails generate model User username:string email:string password_digest:string role:string
```
The user data model has been meticulously designed and implemented within the application to capture essential attributes and information about users. The User model serves as the cornerstone for managing user-related data.

User Model Definition:
In the app/models/user.rb file, the User model is defined with the necessary attributes to comprehensively represent user information.

```# app/models/user.rb
class User < ApplicationRecord
  # Attributes:
  # - username: String
  # - email: String
  # - password_digest: String (for secure password storage)
  # - other_attributes: Data type

  # Associations, Validations, and Methods may also be defined here based on project requirements.

end
```
# 5. User Registration and Management:
### Attributes:
1. Username (username):

Data Type: String
Description: Represents the unique identifier chosen by the user for authentication and display purposes.
2. Email (email):

Data Type: String
Description: Captures the user's email address, serving as a unique identifier for communication and login.
3. Password Digest (password_digest):

Data Type: String
Description: Stores the securely hashed representation of the user's password, enhancing security.
4. Other Attributes:

Additional attributes as required by the application's user data model.
Associations (Optional):
Associations with other models, such as user roles or relationships, may be defined based on the project's requirements.
5. Validations (Optional):
Necessary validations, such as uniqueness constraints or format validations, may be applied to ensure data integrity.
Methods (Optional):
Custom methods may be implemented within the User model to perform specific actions or calculations related to user data.

### 5.1 Page Flow:
The page flow for user registration and management is outlined in our project documentation.
To ensure a seamless and user-friendly experience, a detailed sketch of the user registration and management page flow has been created. This sketch outlines the steps and interactions users will encounter during the registration process and subsequent management of their accounts.

### User Registration Page Flow:
1. Homepage:
Users land on the homepage and are presented with a clear call-to-action to register.

2. Registration Form:
Clicking on the registration link takes users to the registration form.
The form includes fields for essential information such as username, email, and password.
User-friendly validations and error messages are implemented for a smooth submission process.

3. Successful Registration:
Upon successful registration, users receive a confirmation message.
Optionally, users may be redirected to a welcome page or their user dashboard.

### User Management Page Flow:
1. User Dashboard:
After logging in, users are directed to their personalized dashboard.
The dashboard provides an overview of their account status, challenges, and achievements.

2. Profile Editing:
Users can access a profile editing page to update their information.
Changes to username, email, or other relevant details are saved securely.

3. Joining Challenges:
A dedicated section allows users to explore and join sustainability challenges.
Joining a challenge triggers relevant updates on the user's dashboard.

4. Challenge Participation:
Users can actively participate in challenges, track their progress, and view leaderboards.
### 5.2 Test-First and BDD in Rails:
We followed test-driven development principles using RSpec and Capybara for feature tests.

### 5.3 User Administration:
Admin pages for user management were implemented using Rails controllers and views.
Admin User Management Page Flow:
#### Admin Dashboard:
Admins access a specialized dashboard with administrative privileges.

1. View Recently Registered Users:
Admins can navigate to a page displaying recently registered users.
User details and registration timestamps are accessible for review.

2. Authoriry a User role:
Admins have the authority to ban a user directly from the admin dashboard.
Banned users receive notifications and are restricted from further participation.

3. View Statistics:
Admins can access a statistical overview of user registrations.
Metrics such as total users and registration trends are presented.
# 6. Issue Tracker and CI Server:

Issues are tracked using the GitLab issue tracker for each task.
- Issue Tracker:
The issue tracker is actively utilized to track tasks and potential enhancements.
Issues are created before initiating any development work, ensuring a systematic approach to problem-solving.
- CI Server Integration:
GitLab CI/CD is seamlessly integrated into the development workflow.
The CI server provides email notifications of broken builds, and test coverage and code quality metrics are reported.

# Build Basic User Registration and Login Pages:
Following the principles of Behavior-Driven Development (BDD), the basic user registration and login pages have been meticulously implemented. These pages are designed to provide users with a straightforward and secure way to register for the platform and log in securely.

#### User Registration Page:
- Behavior-Driven Development (BDD):
The development process is guided by BDD principles, ensuring that the functionality aligns with user expectations.
- SSL Enabled:
SSL (Secure Sockets Layer) is enabled for the entire application, guaranteeing a secure communication channel and preventing password sniffing.

- Registration Form:
A user-friendly registration form is implemented, capturing essential information such as username, email, and password.

- Validation and Error Handling:
BDD-driven validation ensures that the registration form includes necessary validations.
Clear error messages guide users in case of input errors.

- Secure Password Storage:
Passwords are securely stored using industry-standard hashing techniques, enhancing user account security.
#### User Login Page:
- Behavior-Driven Development (BDD):
BDD principles continue to guide the development of the user login page, promoting a user-centric approach.

- SSL Enabled:
SSL is consistently applied to the login page, preserving the security of user credentials during the login process.
- Login Form:
An intuitive login form is implemented, requesting users to enter their credentials for authentication.

- Authentication:
BDD-driven authentication mechanisms ensure the secure verification of user credentials against stored data.

- Session Management:
Secure session management techniques are employed to maintain user login states and enhance the overall user experience.
# Build User Administration Pages:
Utilizing BDD principles, user administration pages have been constructed to empower administrators with effective tools for managing user-related tasks.

### Admin Dashboard:
An administrative dashboard provides a centralized view of essential user-related information and controls.

- View Recently Registered Users:
Admins can navigate to a dedicated page displaying recently registered users.
User details and registration timestamps are presented for efficient review.

- Authority a User role:
Admins possess the authority to ban a user directly from the admin dashboard.
The banning action triggers appropriate notifications, and banned users face restrictions as intended.

- View User Registration Statistics:
Admins can access a statistics page offering insights into user registration trends and metrics.

# 9. KISS Principle:
We adhered to the KISS (Keep It Simple, Stupid) principle throughout the development process, prioritizing simplicity, clarity, and maintainability.

# 10. Final Steps:
### 10.1 Index Page:
An index page is created, pointing to "Basics" and project documentation.

### 10.2 Check-in Verification:
Code and documentation are routinely checked into GitLab, ensuring version control and collaboration.

> By embracing best practices, adhering to the KISS principle, and leveraging powerful tools, our full-stack application is well-positioned for scalability, maintainability, and continued development. This journey has not only fortified our understanding of software development practices but has also laid a solid foundation for the success of our project.